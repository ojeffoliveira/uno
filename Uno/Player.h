#pragma once

#include "pch.h"
#include "Carta.h"
#include <vector>
#include <iostream>
#include <string>
#include <array>
#include <cassert>
#include <random>
#include <ctime>
#include "Prob.h"

class Player
{
	int m_ID;
	int m_numeroDeCartas;
	std::vector <Carta> Mao;
	bool m_isHuman;
	std::string m_simbolo;
	std::string m_simbolo2;
	std::string m_name;
public:
	void PrintCartaNaMesa();
	Player();
	~Player();
	void SetID(int a) { m_ID = a; }
	int GetID() { return m_ID; }
	void SetCarta(Carta cart, int a);
	void PrintMao();
	void SetName(std::string name);
	std::string GetName();
	void SetMaoSize(int &numeroDeCartas);
	Carta Jogar(std::vector<Player> &Jogadores, int &ProximoJog, Carta &ultima,
				Carta::Cor Cor, bool &jogou, bool &comprou, 
				Carta *proxima, int &contParaComprar, bool &BGBotGame);
	int GetMaoSize();
	Carta::Cor EscolherCor(Carta &ultima, Carta &Penultima, Carta::Cor &aCor, bool &escolheu, bool &jogou);
	void SetHuman(bool Human);
	bool GetHuman();
	void TirarCartaDaMao(int index);
	std::string GetSimbolo();
	void DigiteZero(int &number, int min, int max,
		std::string ErrorMessageOutOfRange = "\n   Este numero e muito grande ou muito pequeno, tente novamente:\n",
		std::string ErrorMessageFail = "\n   Tente novamente: \n");
	std::string GetSimbolo2();
	void SetSimbolo(std::string a, std::string b);
	bool PossoJogar(Carta &ultima, Carta &esta, Carta::Cor cor,int &contParaComprar);
	void ComprarCarta(Carta *&carta, int count);
	Prob::Ter NumToTer(const Carta::Numero &a);
	Prob GetProb(std::vector<std::vector<Prob>*> &ProbPlayer,  std::vector<Player> &Players, Prob::Ter posse);
	std::vector<Carta>::size_type STVC(int a); //Retorna o numero em size std::vector<Carta>::size_type 
	std::vector<Player>::size_type STVP(int a); //Retorna o numero em size std::vector<Player>::size_type 
	//std::array<Carta, 108>::size_type STAC(int a); //Retorna o numero em std::array<Carta, 108>::size_type 
	std::vector<int> PassaOsIndexes(Carta::Numero Num);
	void AtaqueIndex(std::vector<Player> &Players, int &ProximoJog, 
					Carta::Numero Num, std::vector<int> &AtaqueIndexes);
	void ProbDefesa( std::vector<Player> &Players, const int &ProximoJog,
					std::vector<int> &PotDamage, int &PotDamageMe,
					const  Carta::Numero Num, std::vector<int> &ProbDefVoltar);
	void ProbDefesa( std::vector<Player> &Players, const int &ProximoJog,
		const  Carta::Numero Num, std::vector<int> &ProbDefVoltar);
	bool TemCarta(Carta::Numero Num, Carta::Cor color);
	bool TemCarta(Carta::Cor color);
	bool TemCarta(Carta::Numero Num);
	int IndexDaCarta(Carta::Numero Num, Carta::Cor color);
	void SwapCartas(Carta &a, Carta &b);
	void OrgMaoD(); //Organiza a mao do npc para facilitar a jogada
	Carta AI(std::vector<Player> &Jogadores, int &ProximoJog, Carta &ultima,
			Carta::Cor Cor, bool &jogou, bool &comprou, 
			Carta *proxima, int contParaComprar, bool &BGBotGame);
	void SwapInt(int &a, int &b);
	bool Org2(Carta &a, Carta &b, std::array<std::array<int, 2>, 4> &Cores);
	bool Org3(Carta &a, Carta &b, std::array<std::array<int, 2>, 15> &Numeros);
	int GetIndexCor(std::array<std::array < int, 2>, 4> &a, Carta::Cor aCor);
	int GetIndexNum(std::array<std::array < int, 2>, 15> &Numeros, Carta::Numero oNum);
	std::array<std::array<int, 2>, 15>::size_type STAN(int a);
	std::array<std::array<int,2>, 4>::size_type STAC(int a); 
};

