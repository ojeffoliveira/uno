#pragma once
#include "pch.h"

class Carta
{
	
public:
	enum Numero {
		N_0,
		N_1,
		N_2,
		N_3,
		N_4,
		N_5,
		N_6,
		N_7,
		N_8,
		N_9,
		N_PULAR,
		N_INVERTER,
		N_COMPRAR2,
		N_PRETA_COMPRAR4,
		N_PRETA_ESCOLHER_COR,
		N_MAX,
		N_NULO,
	};
	enum Cor {
		COR_AZUL,
		COR_VERMELHO,
		COR_VERDE,
		COR_AMARELO,
		COR_PRETA,
		COR_MAX,
		COR_NULO,
	};
	enum Tipo {
		TIPO_NUMERICA,
		TIPO_ACAO_COR,
		TIPO_ACAO,
		TIPO_MAX,
		TIPO_NULO,
	};
private:
	Tipo m_tipo;
	Numero m_numero;
	Cor m_cor;
public:
	void SetCor(Cor cor);
	void SetNumero(Numero num);
	void SetCarta(Tipo tipo, Numero numero);
	Carta(Tipo tipo, Numero numero, Cor cor);
	void SetTipo(Tipo tipo);
	Carta();
	~Carta();
	Tipo GetTipo();
	Numero GetNumero();
	Cor GetCor();
	void SetNula();
	void PrintCarta();
};

