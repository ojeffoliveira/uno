#pragma once
#include <chrono>

class Timer
{
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1>>;

	std::chrono::time_point<clock_t> m_beg;

public:


	Timer();
	
	void Reset();

	double TempoPassado();
	~Timer();
};

