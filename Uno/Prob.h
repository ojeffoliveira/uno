#pragma once
#include "Fracao.h"

class Prob
{
public:
	enum Ter{
		TER_UMA_COR,
		TER_DUAS_CORES,
		TER_TRES_CORES,
		TER_QUATRO_CORES,
		TER_N_PULAR,
		TER_N_INVERTER,
		TER_N_COMPRAR2,
		TER_N_PRETA_COMPRAR4,
		TER_N_PRETA_ESCOLHER_COR,
		TER_MAX,
		TER_NULO,
	};
	enum NaoTer {
		NTER_UMA_COR,
		NTER_DUAS_CORES,
		NTER_TRES_CORES,
		NTER_QUATRO_CORES,
		NTER_N_PULAR,
		NTER_N_INVERTER,
		NTER_N_COMPRAR2,
		NTER_N_PRETA_COMPRAR4,
		NTER_N_PRETA_ESCOLHER_COR,
		NTER_MAX,
		NTER_NULO,
	};
	enum QuantMaoAcaoCor {
		Q_1,
		Q_2,
		Q_3,
		Q_4,
		Q_5,
		Q_6,
		Q_7,
		Q_8,
		Q_9,
		Q_10,
		Q_11,
		Q_12,
		Q_13,
		Q_14,
		Q_15,
		Q_16,
		Q_17,
		Q_18,
		Q_19,
		Q_20,
		Q_21,
		Q_22,
		Q_23,
		Q_24,
		Q_25,
		Q_TODAS,
	};
	friend Prob operator/( Prob &a,  Prob &b);
	friend Prob operator-( Prob &a,  Prob &b);
	friend Prob operator*( Prob &a,  Prob &b);
	friend Prob operator+( Prob &a,  Prob &b);
	Prob();
	Prob(Ter ter, int quantNaMao, int MaoSize, int baralho, int SomaMSoutros, int SomaQNMoutros);
	Prob(const int &x, const int &y, const int &a, const int &b);
	Prob(Ter ter, int MaoSize, int baralho);
	Prob(NaoTer nter, int MaoSize, int baralho);
	Prob(Ter ter, int quantNaMao, int MaoSize, int baralho);
	void InputProb();
	int QuantNoBaralho(Ter ter);
	int QuantNoBaralho(NaoTer nTer);
	~Prob();
	Prob ReturnMe();
	Fracao& GetFracao() { return m_Probabilidade; }
	friend Prob operator/(Prob &a, int &b);
	void TurnToTh();

private:
	Prob(Fracao a, Ter ter);
	Ter m_ter{ Ter::TER_NULO };
	NaoTer m_naoTer{ NaoTer::NTER_NULO };
	Fracao m_Probabilidade;
	Fracao m_C_universo;
	Fracao m_permuta;


};

