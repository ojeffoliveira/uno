#include "pch.h"
#include "Timer.h"


Timer::Timer(): m_beg(clock_t::now())
{
}

double Timer::TempoPassado() { return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count(); }
void Timer::Reset() { m_beg = clock_t::now(); }

Timer::~Timer()
{
}
