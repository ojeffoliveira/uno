#pragma once
#include <array>
#include "pch.h"
#include "Carta.h"
#include "Player.h"
#include <vector>
#include <array>
#include <random>
#include <ctime>
#include <iostream>
#include <chrono>
#include <ctime>
#include <time.h>
#include <cassert>
#include "Game.h" 



class Game
{
	int m_numeroPlayers; // Numero de jogadores no jogo
	const int m_tamanhoDoDeck; // Define o tamanho do deck, precisa ser m�ltiplo de 108 e precisa ser colocado manualmente no array abaixo
	std::array<Carta, 108> m_deck; // Deck de todas as cartas, as curingas brancas sao escolher cor
	int m_proxima; //Proxima carta no deck
	std::vector<Carta> m_descarte; //Cartas descartadas
	Carta &m_ultima; //Ultima carta descartada
	int m_numeroDeCartas; //Numero de Cartas inicial por jogador
	int m_direcao;
	std::array<std::string, 22> m_simbolos;
	int m_humanPlayers;
	int m_contParaComprar;

	//Configura��es do jogo
	bool m_BGBotGame{ false };
	int m_NumJogos{ 1 };
	bool m_AcabarNoPrim{ false };
	bool m_TempoPassado{ false };
	bool m_TestarMao{ false };
	
public:
	std::vector<Player> Players;
	/*friend void Player::AtaqueIndex(Game *game, Carta::Numero Num, std::vector<int> &AtaqueIndexes);*/
	Game(int numeroPlayers, int numeroDeCartas, Carta nula, int human, std::vector<Carta> TesteMao,
		bool BGBotGame = false, int NumJogos = 1, bool AcabarNoPrim = false, 
		bool TempoPassado = false, bool TestarMao=false);
	~Game();
	void NulasProFim();
	void SetMaoTeste(std::vector<Carta> &TesteMao);
	void Embaralhar();
	void SwapCartas(Carta &a, Carta &b);
	void DistribuirCartas();
	Carta TirarCarta(int index);
	Carta TirarCarta(bool &comprou);
	void PrintDeck();
	int* GetNumeroDeCartas();
	std::vector<int> Play();
	bool CheckMaos(const int &JogAnterior); // Checar se algu�m tem zero cartas, caso positivo, jogo vai acabar na Play()
	void PrintRodada(int ProximoJog);
	std::vector<Player>& GetPlayers();
	/*void PrintJogRodada(int j, int espacosBase, std::string text, bool antes, bool depois);*/
	void DescartarCarta(Carta carta, bool jogou); 
	Carta& GetUltima();
	Carta& GetPenulti();
	void PrintJogRodada(int prox, int j,int espaco);
	void SetSimbolos();
	void Simbolos();
	std::array<std::string, 22>& GetSimbolos();
	std::string& GetSimbolo(int index);
	int GetContParaComprar();
	std::vector<Carta>::size_type STVC(int a); //Retorna o numero em size std::vector<Carta>::size_type 
	std::vector<Player>::size_type STVP(int a); //Retorna o numero em size std::vector<Player>::size_type 
	std::array<Carta, 108>::size_type STAC(int a); //Retorna o numero em std::array<Carta, 108>::size_type 
	void Reemb();
};

