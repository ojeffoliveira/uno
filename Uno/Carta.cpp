
#include "pch.h"
#include "Carta.h"
#include <iostream>

Carta::Carta(Tipo tipo, Numero numero, Cor cor)
	: m_tipo{tipo}, m_numero{numero}, m_cor{cor}
{
}

Carta::Carta()
{
	m_tipo = TIPO_NULO;
	m_numero = N_NULO;
	m_cor = COR_NULO;
}
Carta::~Carta()
{
}
void Carta::SetCor(Cor cor) {
	m_cor = cor;
}
void Carta::SetCarta(Carta::Tipo tipo, Carta::Numero numero) {
	m_tipo = tipo;
	m_numero = numero;
}
Carta::Tipo Carta::GetTipo() { return m_tipo; }
Carta::Numero Carta::GetNumero() { return m_numero; }
Carta::Cor Carta::GetCor() { return m_cor; }

void Carta::SetNumero(Numero num) {
	m_numero = num;
}
void Carta::SetTipo(Tipo tipo) {
	m_tipo = tipo;
}

void Carta::SetNula() {
	m_tipo = Carta::Tipo::TIPO_NULO;
	m_numero = Carta::Numero::N_NULO;
	m_cor = Carta::Cor::COR_NULO;
}

void Carta::PrintCarta() {
	switch (m_numero)
	{
	case Numero::N_0:
		std::cout << " 0";
		break;
	case Numero::N_1:
		std::cout << " 1";
		break;
	case Numero::N_2:
		std::cout << " 2";
		break;
	case Numero::N_3:
		std::cout << " 3";
		break;
	case Numero::N_4:
		std::cout << " 4";
		break;
	case Numero::N_5:
		std::cout << " 5";
		break;
	case Numero::N_6:
		std::cout << " 6";
		break;
	case Numero::N_7:
		std::cout << " 7";
		break;
	case Numero::N_8:
		std::cout << " 8";
		break;
	case Numero::N_9:
		std::cout << " 9";
		break;
	case Numero::N_PULAR:
		std::cout << " @";
		break;
	case Numero::N_INVERTER:
		std::cout << "<>";
		break;
	case Numero::N_COMPRAR2:
		std::cout << "+2";
		break;
	case Numero::N_PRETA_COMPRAR4:
		std::cout << "+4";
		break;
	case Numero::N_PRETA_ESCOLHER_COR:
		std::cout << " *";
		break;
	case Numero::N_NULO:
		std::cout << "??";
		break;
	default:
		std::cout << "!!";
		break;
	}

	switch (m_cor)
	{

	case Cor::COR_AZUL:
		std::cout << " - AZUL";
		break;
	case Cor::COR_VERMELHO:
		std::cout << " - VERMELHO";
		break;
	case Cor::COR_VERDE:
		std::cout << " - VERDE";
		break;
	case Cor::COR_AMARELO:
		std::cout << " - AMARELO";
		break;
	case Cor::COR_PRETA:
		std::cout << " - PRETA";
		break;
	case Cor::COR_NULO:
		std::cout << " - ????";
		break;
	default:
		std::cout << "!!";
		break;
	}
	std::cout << "\n";
}
