#include "pch.h"
#include "Fracao.h"
#include <vector>
#include <iostream>
#include <cassert>

/////////////////////////////////////////////////////////////////
//O construtor default define a fra��o para 1/1
/////////////////////////////////////////////////////////////////
Fracao::Fracao()
{
	m_nume.resize(m_numeLength);
	m_deno.resize(m_denoLength);
	m_nume[0] = 1;
	m_deno[0] = 1;
}

//As opera��es matem�ticas funcionam como seria esperado delas
Fracao operator+( Fracao &a,  Fracao &b) {
	std::vector<int> numeC;
	std::vector<int> denoC;
	//Fatorar os elementos da adi��o
	denoC.resize(a.m_deno.size() + b.m_deno.size());

	for (int i{ 0 }; i < denoC.size(); i++) {
		if (i < a.m_deno.size())
			denoC[i] = a.m_deno[i];
		else
			denoC[i] = b.m_deno[i - a.m_deno.size()];
	}
	

	std::vector<int> tempNumeA;
	std::vector<int> tempNumeB;
	tempNumeA.resize(a.m_nume.size() + b.m_deno.size());
	tempNumeB.resize(b.m_nume.size() + a.m_deno.size());

	int lengthA = a.m_nume.size() + b.m_deno.size();
	int lengthB = b.m_nume.size() + a.m_deno.size();

	for (int i{ 0 }; i < lengthA; i++) {
		if (i < a.m_nume.size())
			tempNumeA[i]= a.m_nume[i];
		else
			tempNumeA[i] = b.m_deno[i - a.m_nume.size()];
	}

	for (int i{ 0 }; i < lengthB; i++) {
		if (i < b.m_nume.size())
			tempNumeB[i] = b.m_nume[i];
		else
			tempNumeB[i] = a.m_deno[i - b.m_nume.size()];
	}
	
	a.FatoreVector(tempNumeA, lengthA);
	b.FatoreVector(tempNumeB, lengthB);
	a.SomeEstes(tempNumeA, tempNumeB, numeC);

	Fracao c(numeC, denoC);
	return c;
}
Fracao operator-( Fracao &a,  Fracao &b) {
	std::vector<int> tempNumeA;
	std::vector<int> tempNumeB;
	std::vector<int> numeC;
	std::vector<int> denoC;
	//Fatorar os elementos da adi��o
	denoC.resize(a.m_deno.size() + b.m_deno.size());

	for (int i{ 0 }; i < denoC.size(); i++) {
		if (i < a.m_deno.size())
			denoC[i] = a.m_deno[i];
		else
			denoC[i] = b.m_deno[i - a.m_deno.size()];
	}

	tempNumeA.resize(a.m_nume.size() + b.m_deno.size());
	tempNumeB.resize(b.m_nume.size() + a.m_deno.size());

	int lengthA = a.m_nume.size() + b.m_deno.size();
	int lengthB = b.m_nume.size() + a.m_deno.size();

	for (int i{ 0 }; i < lengthA; i++) {
		if (i < a.m_nume.size())
			tempNumeA[i] = a.m_nume[i];
		else
			tempNumeA[i] = b.m_deno[i - a.m_nume.size()];
	}

	for (int i{ 0 }; i < lengthB; i++) {
		if (i < b.m_nume.size())
			tempNumeB[i] = b.m_nume[i];
		else
			tempNumeB[i] = a.m_deno[i - b.m_nume.size()];
	}
	tempNumeB.resize((tempNumeB.size() + 1), -1);
	lengthB++;

	a.FatoreVector(tempNumeA, lengthA);
	b.FatoreVector(tempNumeB, lengthB);
	a.SomeEstes(tempNumeA, tempNumeB, numeC);

	Fracao c(numeC, denoC);
	c.Simplificar();
	return c;
}
/////////////////////////////////////////////////////////////////
//
//Esse construtor define uma fra��o com apenas um numerador e um
//denominador
//
/////////////////////////////////////////////////////////////////
Fracao::Fracao(int numerador, int denominador)
{
	m_nume.resize(1);
	m_deno.resize(1);
	m_nume[0] = numerador;
	m_deno[0] = denominador;
}
Fracao operator*(const Fracao &a, const Fracao &b) {
	std::vector<int> numeC;
	std::vector<int> denoC;

	numeC.resize(a.m_nume.size() + b.m_nume.size());
	denoC.resize(a.m_deno.size() + b.m_deno.size());
	for (int i{ 0 }; i < numeC.size(); i++) {
		if (i<a.m_nume.size())
			numeC[i] = a.m_nume[i];
		else
			numeC[i] = b.m_nume[i- a.m_nume.size()];
	}

	for (int i{ 0 }; i < denoC.size(); i++) {
		if (i < a.m_deno.size())
			denoC[i] = a.m_deno[i];
		else
			denoC[i] = b.m_deno[i - a.m_deno.size()];
	}

	Fracao c(numeC, denoC);
	
	return c;
}
Fracao operator/(const Fracao &a, const Fracao &b) {
	std::vector<int> numeC;
	std::vector<int> denoC;

	numeC.resize(a.m_nume.size() + b.m_deno.size());
	denoC.resize(a.m_deno.size() + b.m_nume.size());
	for (int i{ 0 }; i < numeC.size(); i++) {
		if (i < a.m_nume.size())
			numeC[i] = a.m_nume[i];
		else
			numeC[i] = b.m_deno[i - a.m_nume.size()];
	}

	for (int i{ 0 }; i < denoC.size(); i++) {
		if (i < a.m_deno.size())
			denoC[i] = a.m_deno[i];
		else
			denoC[i] = b.m_nume[i - a.m_deno.size()];
	}

	Fracao c(numeC, denoC);
	
	return c;
}
/////////////////////////////////////////////////////////////////
//
//Esse construtor recebe duas sequ�ncias de n�meros em dois vectors
//que ser�o o numerador e o denominador da fra��o. 
//
/////////////////////////////////////////////////////////////////
Fracao::Fracao(std::vector<int> &numerador, std::vector<int> &denominador) {
	SetFracao(numerador, denominador);
}
/////////////////////////////////////////////////////////////////
//Essa fun��o sobreescreve a fra��o anterior com novos valores
/////////////////////////////////////////////////////////////////
void Fracao::SetFracao(int numerador, int denominador) {
	m_numeLength = 1;
	m_denoLength = 1;

	m_nume.resize(1);
	m_deno.resize(1);
	m_nume[0] = numerador;
	m_deno[0] = denominador;
	Simplificar();
}
void Fracao::SetFracao(std::vector<int> &numerador, std::vector<int> &denominador) {
	m_numeLength = numerador.size();
	m_denoLength = denominador.size();

	m_nume.resize(numerador.size());
	m_deno.resize(denominador.size());
	m_nume = numerador;
	m_deno = denominador;
	Simplificar();
}
void Fracao::AddNume(int a) {
	m_nume.resize((m_nume.size()+1),a);
}
void Fracao::AddDeno(int a) {
	m_deno.resize((m_deno.size() + 1), a);
	m_numeLength += 1;
}
//Essa fun��o recebe dois vectors, cada um inclui fatores de uma multiplic�p
//Mas eles(ou o resultado da multiplic�o) em si s�o fatores de uma adi��o
//A fun��o efetua a soma de forma facilitada usando as propriedades da adi��o
void Fracao::SomeEstes(std::vector<int> &a, std::vector<int> &b, std::vector<int> &c) {
	//O for seguinte corta os valores que estejam em ambos os vectors e coloca
	// o valor no vector c que ser� o resultado final.
	for (int i{ 0 }; i < a.size();i++) {
		for (int j{ 0 }; j < b.size(); j++) {
			if (a[i] == b[j]) {
				c.resize(c.size() + 1, a[i]);
				a[i] = 1;
				b[j] = 1;
			}
		}
	}
	//Os dois vectors seguintes efetuam a mult dos n�meros restantes 
	//(que n�o estavam presentes em ambos os vetores)
	int multA{ 1 }, multB{ 1 };
	for (int i{ 0 }; i < a.size(); i++)
		multA *= a[i];
	for (int i{ 0 }; i < b.size(); i++)
		multB *= b[i];

	c.resize(c.size() + 1, (multA + multB));
}
Fracao::~Fracao()
{
}
void Fracao::FatoreEste(std::vector<int> &Numeros, const int &index, int &Length) {
	//O primeiro for itera por todos os elementos do vector
	//O segundo checa se cada elemento � divis�vel por 2, depois 3
	// at� a metade do n�mero.
	//O algoritmo ali embaixo faz o mesmo s� que para n�meros positivos	
	assert(index<Numeros.size() && "FatoreEste() maldito index maior do que deve ser");
		/*if (Numeros[index] == 0) {
			Numeros[index] = 1;
			return;
		}*/
		if (Numeros[index]<-1) {
			Numeros[index] = ((Numeros[index]) / -1);
			Numeros.resize(Numeros.size() + STVI(1));
			Length++;
			Numeros[Numeros.size() - STVI(1)] = -1;
		}

		/*int limite{ -1 * (Numeros[index] / 2) };
		for (int j{ -1 }; j < limite; j--) {
			while (Numeros[index] % j == 0 && j < limite) {
				Numeros[index] = (Numeros[index] / j);
				Numeros.resize(Numeros.size() + STVI(1));
				Length++;
				Numeros[Numeros.size() - STVI(1)] = j;
				limite = (Numeros[index] / 2)*-1;
			}

		}*/
	
	
		int limite{ ((Numeros[index]) / 2) };
		
		if (Numeros[index] != -1 && Numeros[index] != 1
			&& Numeros[index] != 2 && Numeros[index] != 3
			&& Numeros[index] != 5 && Numeros[index] != 7
			&& Numeros[index] != 11 && Numeros[index] != 13
			&& Numeros[index] != 17 && Numeros[index] != 19
			&& Numeros[index] != 23 && Numeros[index] != 29
			&& Numeros[index] != 31 && Numeros[index] != 37
			&& Numeros[index] != 41 && Numeros[index] != 59
			&& Numeros[index] != 43 && Numeros[index] != 61
			&& Numeros[index] != 47 && Numeros[index] != 67
			&& Numeros[index] != 53 && Numeros[index] != 71
			&& Numeros[index] != 73 && Numeros[index] != 89
			&& Numeros[index] != 79 && Numeros[index] != 97
			&& Numeros[index] != 83 && Numeros[index] != 101
			&& Numeros[index] != 103 && Numeros[index] != 113
			&& Numeros[index] != 107 && Numeros[index] != 127
			&& Numeros[index] != 109 && Numeros[index] != 131
			) {
			
			for (int j{ 2 }; j <= limite; j++) {
				while (Numeros[index] % j == 0 && j <= limite) {
					Numeros[index] = (Numeros[index] / j);
					Numeros.resize(STVI(Length+1), j);
					Length++;
					limite = (Numeros[index] / 2);
				}
			}
		}
}
void Fracao::FatoreVector(std::vector<int> &Vector, int &Length) {
	int lengthInitial{ Length };
	for (int i{ 0 }; i < lengthInitial; i++) {
		FatoreEste(Vector, i, Length);
	}
}
//Esta fun��o Fatora todos os membros do numerador
void Fracao::FatorarNume() {
	
	FatoreVector(m_nume, m_numeLength);
	
	for (int i{ 0 }; i < m_numeLength; i++) {
		bool swap{ false };
		for (int j{0}; j < m_numeLength-i-1; j++) {
			if (m_nume[j]> m_nume[j+1]) {
				Swap(m_nume[j+1], m_nume[j]);
				swap = true;
			}
		}
		if (!swap)
			break;
	}
	
}
//Esta fun��o Fatora todos os membros do denominador
void Fracao::FatorarDeno() {

	FatoreVector(m_deno, m_denoLength);

	for (int i{ 0 }; i < m_denoLength; i++) {
		bool swap{ false };
		for (int j{ 0 }; j < m_denoLength - i - 1; j++) {
			if (m_deno[j] > m_deno[j + 1]) {
				Swap(m_deno[j + 1], m_deno[j]);
				swap = true;
			}
		}
		if (!swap)
			break;
	}

}
//Esta fun��o Fatora tanto o numerador quanto o denominador
//E checa se h� valores iguais entre eles, caso sim, eles s�o cortados 
void Fracao::Simplificar() {
	FatorarNume();
	FatorarDeno();

	//Conta quantos 1 tem
	int contUm{ 0 }, inicio{0};
	//Depois de fatorar os numeros, os multiplicadores iguais s�o cortados
	for (int n{ 0 }; n < m_numeLength; n++) {
		for (int d{inicio}; d < m_denoLength; d++) {
			if (m_nume[n] == m_deno[d]) {
				m_nume[n] = 1;
				m_deno[d] = 1;
				inicio = d+1;
				contUm++;
				break;
			}
		}	
	}
	
	//selection Sort para colocar os 1's no fim dos vectors para poder cortalos.
	for (int i{ 0 }; i < m_numeLength; i++) {
		int Fim{ 1 };
		if (m_nume[i] == 1) {
			for (int j{ m_numeLength - Fim }; j > i; j--) {

				if (m_nume[i] == 1 && m_nume[j] != 1) {
					Swap(m_nume[j], m_nume[i]);
					Fim++;
					break;
				}
			}
		}
	}
	
	for (int i{ 0 }; i < m_denoLength; i++) {
		int Fim{ 1 };
		if (m_deno[i] == 1) {
			for (int j{ m_denoLength - Fim }; j > i; j--) {

				if (m_deno[i] == 1 && m_deno[j] != 1) {
					Swap(m_deno[j], m_deno[i]);
					Fim++;
					break;
				}
			}
		}
	}

	if (contUm > 0) {
		m_nume.resize(m_nume.size() - STVI(contUm));
		m_numeLength -= contUm;
		m_deno.resize(m_deno.size() - STVI(contUm));
		m_denoLength -= contUm;
	}
	
	if (m_nume.size() == STVI(0)) {
		m_nume.resize(1, 1);
		m_numeLength = 1;
	}
	if (m_deno.size() == STVI(0)) {
		m_deno.resize(1, 1);
		m_denoLength = 1;
	}

}

void Fracao::Swap(int &a, int &b) {
	int temp = b;
	b = a;
	a = temp;
}
//Printa o resultado da divis�o do numerador pelo denominador
double Fracao::PrintValor() {
	double nume{1};
	for (int n{ 0 }; n < m_numeLength; n++) {
		nume *= m_nume[n];
	}
	double deno{1};
	for (int d{ 0 }; d < m_denoLength; d++) {
		deno*= m_deno[d];
	}
		
	double valor = ((nume)/(deno));
	return valor;
}
//Printa o valor vezes cem
void Fracao::PrintPorcen() {
	double valor(PrintValor());

	valor *= 100;
	std::cout << "\n   Porcentagem "<<valor << "\n";
}
//Printa os elementos da fra��o, primeiro o numerador, depois o denominador
void Fracao::PrintFracao() {
	std::cout << "\n   ";
	for (int n{ 0 }; n < m_numeLength; n++) {
		std::cout << m_nume[n];
		if (n < m_numeLength - 1)
			std::cout << " * ";
	}
	std::cout << "/\n   ";
	
	for (int d{ 0 }; d < m_denoLength; d++) {
		std::cout << m_deno[d];
		if (d < m_denoLength - 1)
			std::cout << " * ";
	}
	std::cout << "\n";
}