﻿Jogo de UNO no console. Execute Uno.exe na pasta Release.

Possível jogar contra o computador, ou contra outras pessoas, localmente.
Numero de jogadores, humanos ou nao, só é limitado pelo numero de 
cartas (108).

==================================================================================

Se encontrar um erro, envie um print screen para jefferson123oliveira@gmail.com
==================================================================================
Desenvolvimento futuro: 

Mudar a função que organiza as carta (para a IA e para o jogador tbm) para que use 
grafos.

Aprimorar o algoritmo pra que ele consiga tomar acoes
com base no ambiente (numero de cartas na mao de outros jogadores,
ou, uma aproximacao do numero) e nao so com base na carta na mesa.

Aprimorar o algoritmo pra que ele consiga tomar acoes
com base no historico de jogadas de outros jogadores.

==================================================================================
=
=   Simbolos usados no jogo:
=    @ - pular o proximo jogardor
=   <> - inverter a direcao do jogo
=    * - coringa/escolher cor
=   Os numeros na tela mostram a quantidade de cartas na mao de cada jogador.
=   Os > < indicam o jogador da rodada, por exemplo: >7<.
=   Os outros simbolos em torno dos numeros ([], {}, !!) indicam cada jogador humano.
=
==================================================================================
